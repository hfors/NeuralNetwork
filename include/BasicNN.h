#pragma once

#include <vector>
#include <deque>

#include <Eigen/Dense>

using Eigen::MatrixXd;

class BasicNN
{
public:
	static MatrixXd forward_propagate(const BasicNN& bnn, MatrixXd input);
	// Returns at vector containing all layer activations
	static std::vector<MatrixXd> forward_propagate_all(const BasicNN& bnn, MatrixXd input);
	static double cost_function(const BasicNN& bnn, MatrixXd input, MatrixXd output, const double lambda);
	// Returns a vector containing the cost function jacobian w.r.t each weight
	static std::vector<MatrixXd> backward_propagate(const BasicNN& bnn, MatrixXd input, MatrixXd output, const double lambda);

public:
	BasicNN() = default;
	BasicNN(std::vector<int>);

public:
	const std::vector<int>& get_dimensions() const;
	const std::vector<MatrixXd>& get_weights() const;
	std::vector<MatrixXd>& set_weights(std::vector<MatrixXd>);

private:
	std::vector<MatrixXd> m_layer_weights;
	std::vector<int> m_dimensions;
};
