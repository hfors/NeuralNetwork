#include "BasicNN.h"
#include <iostream>

const std::vector<int>& BasicNN::get_dimensions() const
{
	return m_dimensions;
}

const std::vector<Eigen::MatrixXd>& BasicNN::get_weights() const
{
	return m_layer_weights;
}

std::vector<Eigen::MatrixXd>& BasicNN::set_weights(std::vector<MatrixXd> weights)
{
	m_layer_weights = weights;
	return m_layer_weights;
}

BasicNN::BasicNN(std::vector<int> dimensions)
	: m_dimensions(std::move(dimensions))
{
	for (auto it = m_dimensions.cbegin(); it != m_dimensions.cend() - 1; ++it) {
		m_layer_weights.push_back(Eigen::MatrixXd(*(it + 1), 1 +*it).setZero());
	}
}

// Returns the output activation
MatrixXd BasicNN::forward_propagate(const BasicNN& bnn, MatrixXd input)
{
	assert(input.rows() == bnn.get_dimensions().front() && "input data size doesn't agree with input layer size");

	for (const auto& w : bnn.get_weights()) {
		input.conservativeResize(input.rows() + 1, Eigen::NoChange);
		input.row(input.rows() - 1) = MatrixXd(1, input.cols()).setOnes();

		assert(w.cols() == input.rows() && "mismatch between weight matrix and hidden layer input size");
		input = (w*input).unaryExpr([](double x) {
			return 1 / (1 + std::exp(-x)); 
		}).eval();
	}
	return input;
}

// Returns all layer activations. Not just the output activation.
std::vector<MatrixXd> BasicNN::forward_propagate_all(const BasicNN& bnn, MatrixXd input)
{
	std::vector<MatrixXd> actv;
	actv.push_back(std::move(input));
	for (const auto& w : bnn.get_weights()) {
		MatrixXd a = actv.back();

		a.conservativeResize(a.rows() + 1, Eigen::NoChange);
		a.row(a.rows() - 1) = MatrixXd(1, a.cols()).setOnes();

		assert(w.cols() == a.rows() && "mismatch between weight matrix and hidden layer input size");
		a = (w*a).unaryExpr([](double x) {
			return 1 / (1 + std::exp(-x));
		}).eval();
		actv.push_back(std::move(a));
	}
	return actv;
}

double BasicNN::cost_function(const BasicNN& bnn, MatrixXd input, MatrixXd output, const double lambda)
{
	const MatrixXd actv = forward_propagate(bnn, input);

	double cost_log = 0;
	const auto m = input.cols();

	const MatrixXd& y = output;
	const MatrixXd y_neg = MatrixXd(y.rows(), y.cols()).setOnes() - output;

	const MatrixXd h = actv.array().log();
	const MatrixXd h_neg = (MatrixXd(h.rows(), h.cols()).setOnes() - actv).array().log();

	MatrixXd cost_expr = y.cwiseProduct(h) + y_neg.cwiseProduct(h_neg);
	cost_log = -cost_expr.sum()/m;

	double cost_w = 0;
	for (const auto& w : bnn.get_weights()) {
		cost_w += w.cwiseAbs2().sum();
	}
	cost_w *= lambda / (2 * m);

	return cost_log + cost_w;
}


std::vector<Eigen::MatrixXd> BasicNN::backward_propagate(const BasicNN& bnn, MatrixXd input, MatrixXd output, const double lambda)
{
	std::vector<MatrixXd> cost_pd; // Cost function partial derivate
	const auto& weights = bnn.get_weights();
	for (const auto& w : weights) {
		cost_pd.push_back(MatrixXd(w.rows(), w.cols()).setZero());
	}

	// Find all the layer activations through forward propagation
	std::vector<MatrixXd> actv = forward_propagate_all(bnn, input);
	// Add bias units
	for (auto it = actv.begin(); it != actv.end() - 1; ++it) {
		it->conservativeResize(it->rows() + 1, Eigen::NoChange);
		it->row(it->rows() - 1) = MatrixXd(1, it->cols()).setOnes();
	}

	MatrixXd old_delta;
	const auto m = input.cols(); // Number of test samples
	for (auto it = actv.rbegin()+1; it != actv.rend(); ++it) {
		auto j = actv.rend()-it;
		j -= 1;

		// Find the error term for the layer
		MatrixXd delta;
		if (it != actv.rbegin()+1) {
			delta = weights[j+1].transpose() * old_delta.topRows(weights[j+1].rows());
			delta.cwiseProduct(*(it-1)).cwiseProduct(MatrixXd(delta.rows(), delta.cols()).setOnes() - *(it-1));
		}
		else
			delta = actv.back() - output;

		// Compute the addition to the cost function partial derivative
		MatrixXd big_delta = delta * it->transpose();
		if (it != actv.rbegin()+1)
			big_delta.conservativeResize(big_delta.rows() - 1, Eigen::NoChange);
		cost_pd[j] +=(big_delta);
		if (it != actv.rend())
			cost_pd[j] += (weights[j] * lambda);
		cost_pd[j] /= m;

		old_delta.resize(delta.rows(), Eigen::NoChange);
		old_delta = delta;
	}
	return cost_pd;
}