#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "BasicNN.h"
#include <Eigen/Dense>

#include <iostream>
#include <algorithm>
#include <numeric>

#define TOL 1e-6 // Floating point tolerance
#define EPS 1e-4 // Derivative approximation epsilon
#define NVAR 10 // Default number of input variables
#define NTEST 10 // Default training set size
#define NLAY 10 // Default number of layers. Including the input and output layer

TEST_CASE("Unit test BasicNN constructors")
{
	BasicNN bnn1;
	REQUIRE(bnn1.get_dimensions().size() == 0);
	REQUIRE(bnn1.get_weights().size() == 0);

	auto weight_check = [](const auto& bnn, const auto& dim) {
		for (auto it = bnn.get_weights().cbegin(); it != bnn.get_weights().cend(); ++it) {
			auto i = it - bnn.get_weights().cbegin();
			if (it->rows() != dim[i + 1] || it->cols() != dim[i] + 1) // dim[i]+1 due to bias unit
				return false;
		}
		return true;
	};

	// A network with only one layer should have no weight matrices
	{
		const BasicNN bnn({ NVAR });
		REQUIRE(bnn.get_weights().size() == 0);
	}

	// Check that the weight matrices agree with the supplied dimensions,
	// accounting for the bias units.
	std::vector<int> dims;
	for (std::size_t i = 0; i < 10; ++i) {
		dims.push_back(i);
		std::random_shuffle(dims.begin(), dims.end());

		REQUIRE(weight_check(BasicNN(dims), dims));
	}
}

// Check that forward propagation functions properly
TEST_CASE("Unit test forward propagation")
{
	using Eigen::MatrixXd;

	// Check that forward propagation matrix operations are consistent dimension-wise
	{
		std::vector<int> dims;
		for (std::size_t i = 1; i < 10; ++i) {
			dims.push_back(i);
			REQUIRE_NOTHROW(BasicNN::forward_propagate(BasicNN(dims), MatrixXd(dims.front(), i).setZero()));
		}
	}

	{
		std::vector<int> dims;
		for (std::size_t i = 1; i < 10; ++i) {
			dims.push_back(i);
			std::random_shuffle(dims.begin(), dims.end());
			REQUIRE_NOTHROW(BasicNN::forward_propagate(BasicNN(dims), MatrixXd(dims.front(), i).setZero()));
		}
	}

	// A network with only one layer should output the input
	{
		const BasicNN bnn({ NVAR });
		const MatrixXd input = MatrixXd::Random(NVAR, NTEST);
		const MatrixXd output = BasicNN::forward_propagate(bnn, input);

		REQUIRE(std::abs((input-output).sum()) < TOL);
	}
}

// Check that back propagation functions properly. This also tests that the cost
// function is correctly implemented, by comparing back prop and the cost function
// to each other
TEST_CASE("Unit test back propagation")
{
	using Eigen::MatrixXd;

	std::vector<int> dims(NLAY);
	std::iota(dims.begin(), dims.end(), 1);
	std::random_shuffle(dims.begin(), dims.end());

	const int input_size = dims.front();
	const int output_size = dims.back();

	BasicNN bnn(dims);
	std::vector<MatrixXd> weights;
	for (const auto& w : bnn.get_weights())
		weights.push_back(MatrixXd::Random(w.rows(), w.cols()));
	bnn.set_weights(weights);

	MatrixXd input = MatrixXd::Random(input_size, NTEST);
	MatrixXd output = MatrixXd::Random(output_size, NTEST);
	
	// Check that back prop does not throw
	REQUIRE_NOTHROW(BasicNN::backward_propagate(bnn, input, output, 0));

	// Check that the cost function and back prop work correctly by comparing the partial
	// derivatives of the cost function to the values given by back prop. Uses the newton
	// quotient to approximate the former.
	const auto jacob_bp = BasicNN::backward_propagate(bnn, input, output, 0);
	bool below_tol = true;
	for (auto it = jacob_bp.cbegin(); it != jacob_bp.cend(); ++it) {
		const auto i = it - jacob_bp.cbegin();
		for (int j = 0; j < weights[i].rows(); ++j) {
			for (int k = 0; k < weights[i].cols(); ++k) {
				auto weights_plus = weights;
				auto weights_minus = weights;
				weights_plus[i](j, k) += EPS;
				weights_minus[i](j, k) -= EPS;

				bnn.set_weights(weights_plus);
				const auto cost_plus = BasicNN::cost_function(bnn, input, output, 0);
				bnn.set_weights(weights_minus);
				const auto cost_minus = BasicNN::cost_function(bnn, input, output, 0);

				auto jacob_cost = (cost_plus - cost_minus) / (2 * EPS);
				below_tol = std::abs((*it)(j, k) - jacob_cost) < TOL;
			}
		}
	}
	REQUIRE(below_tol);
}